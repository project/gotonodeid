
This simple module adds a block that lets a user jump to a specified node page by it's ID.

Inspired by Hanan Cohen's post: http://webster.co.il/2008/12/16/998/ (Hebrew)
This functionality is useful when you want to ask someone offline to visit
a specific page on your website. So instead of writing down a full URL (which can be long,
thanks to aliases) or explaining how to navigate through all of the menus, just give him
the node ID and tell him to use the force... uh... form :-P

Installation
------------
1) Copy the gotonodeid directory to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)

Configuration
-------------

Set permissions for users that can see the current page node ID.

Support
-------

Please post bug reports and feature requests on the module's
drupal.org project page.

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt

Credits / Contact
-----------------

The module was:
- Inspired by Hanan Cohen: http://twitter.com/hananc, http://webster.co.il/
- Developed by Alon Pe'er: http://twitter.com/alonpeer, http://alonpeer.com/